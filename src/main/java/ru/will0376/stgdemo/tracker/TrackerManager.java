package ru.will0376.stgdemo.tracker;

import ru.will0376.stgdemo.database.MysqlDataBase;

/**
 * Класс, отвечающий за трекинги (возможная заготовка к абстракции)
 *
 * @author Will0376
 */
public class TrackerManager {

	/**
	 * Заносит новую запись в базу данных
	 *
	 * @param clanId Id клана
	 * @param userId Id пользователя
	 * @param gold   кол-во золота
	 *
	 * @see MysqlDataBase#addNewTrack(long, long, int)
	 */
	public static void trackerClanGold(long clanId, long userId, int gold) {
		MysqlDataBase.INSTANCE.addNewTrack(clanId, userId, gold);
	}
}
