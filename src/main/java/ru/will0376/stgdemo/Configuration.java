package ru.will0376.stgdemo;

import org.cfg4j.provider.ConfigurationProvider;
import org.cfg4j.provider.ConfigurationProviderBuilder;
import org.cfg4j.source.files.FilesConfigurationSource;
import org.cfg4j.source.reload.strategy.PeriodicalReloadStrategy;
import ru.will0376.stgdemo.database.MysqlDataBase;

import java.nio.file.Paths;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * Класс конфигурации для всего приложения
 *
 * @author Will0376
 */
public class Configuration {
	private static final ConfigurationProvider provider =
			new ConfigurationProviderBuilder().withReloadStrategy(new PeriodicalReloadStrategy(5, TimeUnit.SECONDS))
			.withConfigurationSource(new FilesConfigurationSource(() -> Collections.singletonList(Paths.get(System.getProperty(
					"user.dir"), "application.yaml"))))
			.build();
	public static DatabaseConfig databaseConfig = loadDatabaseConfig();

	/**
	 * Загружает конфигурацию базы данных, возвращая проинициализированный интерфейс {@link DatabaseConfig}
	 *
	 * @return Экземпляр интерфейса
	 */
	private static DatabaseConfig loadDatabaseConfig() {
		return provider.bind("database", DatabaseConfig.class);
	}

	/**
	 * Подготавливает строку хоста для {@link MysqlDataBase#getConnection()} из параметров, которые загружаются cfg4j
	 *
	 * @return готовая строка хоста
	 */
	public static String getReadyHostString() {
		return String.format("%s%s/%s%s", databaseConfig.driverString(), databaseConfig.hostname(), databaseConfig.database(),
				(databaseConfig.connectionSettings()
				                                                                                                                        .isEmpty() ? "" : "?" + databaseConfig.connectionSettings()));
	}

	/**
	 * Интерфейс, в который библиотека cfg4j загружает данные по базе данных (по ключу "database") из файла конфигурации.
	 * <p>Доступ к проинициализированному интерфейсу получать через поле {@link Configuration#databaseConfig}</p>
	 */
	public interface DatabaseConfig {
		String driverString();

		String driverClass();

		String hostname();

		String database();

		String username();

		String password();

		String trackerTableName();

		String connectionSettings();

		String checkTableSQL();

		String newTrackEntrySQL();
	}
}
