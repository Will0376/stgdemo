package ru.will0376.stgdemo.loader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import lombok.extern.log4j.Log4j2;
import ru.will0376.stgdemo.clan.Clan;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Загрузчик кланов из одного .json файла
 *
 * @author Will0376
 */
@Log4j2
public class JsonFileLoader {
	private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private static final Path jsonfile = Paths.get(System.getProperty("user.dir"), "clans.json");
	private List<Clan> clanList = new ArrayList<>();

	/**
	 * Загружает файл по пути {@link JsonFileLoader#jsonfile} в память и парсит его в лист кланов
	 * <p>Если загрузка/парсинг не удался - создаётся пустой лист</p>
	 */
	public void loadList() {
		try {
			Reader reader = Files.newBufferedReader(jsonfile);
			clanList = gson.fromJson(reader, new TypeToken<List<Clan>>() {
			}.getType());
			reader.close();
		} catch (IOException e) {
			clanList = new ArrayList<>();
			log.error("Failed to load clan list.", e);
		}
	}


	/**
	 * Возвращает текущий загруженный лист
	 *
	 * @return лист кланов
	 */
	public List<Clan> getList() {
		return clanList;
	}


	/**
	 * Сохраняет лист кланов на диск, перезаписывая файл если он существует.
	 *
	 * @throws IOException Если не удалось удалить файл и/или записать новый
	 */
	public void saveList() throws IOException {
		Files.deleteIfExists(jsonfile);
		BufferedWriter writer = Files.newBufferedWriter(jsonfile);
		gson.toJson(clanList, writer);
		writer.close();
	}


	/**
	 * Сохраняет экземпляр клана в лист, либо перезаписывает его.
	 *
	 * @param clan Экземпляр клана
	 *
	 * @return true - если всё прошло удачно, иначе - false
	 */
	public boolean saveElement(Clan clan) {
		Clan element = getElement(clan.getId());
		if (element != null)
			clanList.set(clanList.indexOf(element), clan);
		else {
			clanList.add(clan);
		}
		return true;
	}


	/**
	 * Возвращает экземпляр класса клана, если он существует
	 * <p>Является аналогом {@link ru.will0376.stgdemo.clan.ClanManager#getClan(long)}</p>
	 *
	 * @param id ID клана
	 *
	 * @return Экземпляр класса {@link Clan}, иначе null
	 */
	private Clan getElement(long id) {
		return clanList.stream().filter(e -> e.getId() == id).findFirst().orElse(null);
	}
}
