package ru.will0376.stgdemo.database;

import lombok.extern.log4j.Log4j2;
import ru.will0376.stgdemo.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Класс, отвечающий за работу с базой MySQL
 */
@Log4j2
public class MysqlDataBase {
	public static MysqlDataBase INSTANCE = new MysqlDataBase();
	/**
	 * Фиксированный пул потоков, которые могут использоваться для взаимодействия с базой данных
	 */
	private static final ExecutorService executor = Executors.newFixedThreadPool(4);

	public MysqlDataBase() {
		checkTrackTable();
	}

	/**
	 * Подключается к базе по данным из конфига и возвращает экземпляр коннекта.
	 * <p>В случае неудачи подключения - завершает работу программы с кодом ошибки 200</p>
	 *
	 * @return экземпляр подключения к базе
	 * @see Configuration конфигурация
	 */
	public Connection getConnection() {
		Configuration.DatabaseConfig databaseConfig = Configuration.databaseConfig;
		try {
			Class.forName(databaseConfig.driverClass());
			return DriverManager.getConnection(Configuration.getReadyHostString(), databaseConfig.username(),
					databaseConfig.password());
		} catch (ClassNotFoundException e) {
			log.error("Can't find sql driver", e);
		} catch (SQLException e) {
			log.error("An error occurred while trying to connect to the database:", e);
		}

		System.exit(200);
		return null;
	}

	/**
	 * Проверяет в базе данных таблицу из конфига и создаёт её, если она отсутствует.
	 * <p><b>Не создаёт базу данных. Только таблицу в ней</p>
	 * <p>Заменяет шаблон $trackertable$ на название таблицы, получаемое из конфигурации</p>
	 *
	 * @see Configuration конфигурация
	 */
	public void checkTrackTable() {
		try {
			Connection connection = getConnection();
			Configuration.DatabaseConfig databaseConfig = Configuration.databaseConfig;
			connection.prepareStatement(databaseConfig.checkTableSQL()
					.replace("$trackertable$", databaseConfig.trackerTableName())).execute();
			connection.close();
		} catch (Exception e) {
			log.error("Error while checking gold track table", e);
		}
	}

	/**
	 * Заносит в базу данных новую запись об изменении золота в клане.
	 * <p>Используются потоки для избежания блокировки приложения</p>
	 *
	 * @param clanId Id клана, заменяет шаблон $clanId$
	 * @param userId Id пользователя, заменяет шаблон $userId$
	 * @param gold   кол-во, на которое увеличивается/уменьшается золото в клане. Заменяет шаблон $gold$
	 *
	 * @see Configuration конфигурация
	 */
	public void addNewTrack(long clanId, long userId, int gold) {
		executor.submit(() -> {
			try {
				Connection connection = getConnection();
				Configuration.DatabaseConfig databaseConfig = Configuration.databaseConfig;
				String readySQL = databaseConfig.newTrackEntrySQL()
						.replace("$trackertable$", databaseConfig.trackerTableName())
						.replace("$clanId$", String.valueOf(clanId))
						.replace("$userId$", String.valueOf(userId))
						.replace("$gold$", String.valueOf(gold));

				connection.prepareStatement(readySQL).executeUpdate();
				connection.close();
			} catch (Exception e) {
				log.error("Couldn't upload a new track entry", e);
			}
		});
	}
}
