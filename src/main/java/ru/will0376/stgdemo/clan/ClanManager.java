package ru.will0376.stgdemo.clan;

import lombok.extern.log4j.Log4j2;
import ru.will0376.stgdemo.loader.JsonFileLoader;

import java.io.IOException;
import java.util.List;

/**
 * Обобщённый загрузчик кланов (заготовка к абстракции)
 *
 * @author Will0376
 */
@Log4j2
public class ClanManager {
	private static final JsonFileLoader loader = new JsonFileLoader();

	/**
	 * Безопасно загружает кланы с диска
	 * <p>Если происходит ошибка - устанавливается пустой ArrayList</p>
	 */
	public static void loadList() {
		loader.loadList();
	}

	/**
	 * Безопасно сохраняет кланы на диск.
	 */
	public static void saveList() {
		try {
			loader.saveList();
		} catch (IOException e) {
			log.error("Failed to save clan list.", e);
		}
	}

	/**
	 * Создаёт новый экземпляр клана с заданным именем, и с первым доступным ID-ом.
	 * <p>Не сохраняя его в листе</p>
	 *
	 * @param name Название нового клана
	 *
	 * @return Экземпляр нового клана
	 */
	public static Clan createNewClan(String name) {
		return new Clan(0, getFirstAvailableID(), name);
	}

	/**
	 * Находит экземпляр клана по его ID
	 *
	 * @param clanId ID клана
	 *
	 * @return Экземпляр класса {@link Clan} - если он был найден в листе кланов, иначе null.
	 */
	public static Clan getClan(long clanId) {
		return getClanList().stream().filter(e -> e.getId() == clanId).findFirst().orElse(null);
	}

	/**
	 * Проверяет, существует ли клан
	 *
	 * @param clanId ID клана
	 *
	 * @return true - если клан существует и находится в листе кланов, иначе - false.
	 */
	public static boolean isClanExist(long clanId) {
		return getClanList().stream().anyMatch(e -> e.getId() == clanId);
	}

	/**
	 * Возвращает лист экземпляров класса Clan.
	 *
	 * @return Лист кланов
	 */
	public static List<Clan> getClanList() {
		return loader.getList();
	}

	/**
	 * Сохраняет экземпляр клана в листе
	 *
	 * @param clan экземпляр класса {@link Clan}
	 */
	public static void saveClan(Clan clan) {
		loader.getList().add(clan);
		saveClan(clan.getId());
	}

	/**
	 * Сохраняет клан по его ID
	 *
	 * @param clanId ID клана
	 *
	 * @return true - если удалось сохранить, иначе - false.
	 */
	public static boolean saveClan(long clanId) {
		try {
			return loader.saveElement(getClan(clanId));
		} catch (Exception e) {
			log.error("Failed to save clan.", e);
		}
		return false;
	}

	/**
	 * Возвращает первый доступный идентификатор для нового клана
	 *
	 * @return свободный ID
	 */
	public static long getFirstAvailableID() {
		return getClanList().stream().mapToLong(Clan::getId).max().orElse(0) + 1;
	}
}
