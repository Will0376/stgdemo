package ru.will0376.stgdemo.clan;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Класс, хранящий данные клана.
 *
 * @author Will0376
 */
@Getter
@Setter(AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class Clan {
	private volatile int gold;
	private long id;
	private String name;

	protected synchronized int getGold() {
		return this.gold;
	}

	/**
	 * Увеличивает количество доступного золота в текущем клане на входящее значение.
	 * Не позволяет уменьшить валюту меньше нуля.
	 * <p>Можно использовать в потоках</p>
	 *
	 * @param gold кол-во, сколько надо добавить к текущему значению золота.
	 *
	 * @return количество золота после присвоения
	 */
	protected synchronized int incGold(int gold) {
		int prevGold = this.gold;
		this.gold = Math.max(0, prevGold + gold);
		return this.gold;
	}

	/**
	 * Уменьшает количество доступного золота текущего клана на входящее значение
	 * <p>Можно использовать в потоках</p>
	 *
	 * @param gold кол-во золота, которое необходимо убавить
	 *
	 * @return количество золота после присвоения
	 */
	protected synchronized int decGold(int gold) {
		return incGold(-gold);
	}
}