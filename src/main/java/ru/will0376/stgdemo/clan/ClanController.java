package ru.will0376.stgdemo.clan;

import ru.will0376.stgdemo.tracker.TrackerManager;

/**
 * Контроллер, предоставляющий возможность управления кланом
 *
 * @author Will0376
 */
public class ClanController {

	/**
	 * Возвращает количество золота, доступное клану
	 *
	 * @param clanId ID клана
	 *
	 * @return кол-во золота
	 */
	public int getGold(long clanId) {
		return ClanManager.getClan(clanId).getGold();
	}

	/**
	 * Увеличивает кол-во золота в клане по его ID и заносит действие в трекер
	 *
	 * @param clanId ID клана
	 * @param userId ID пользователя
	 * @param gold   кол-во золота, на которое надо увеличить
	 *
	 * @return доступное кол-во золота после увеличения.
	 * @see Clan#incGold(int)
	 */
	public int incGold(long clanId, long userId, int gold) {
		try {
			return ClanManager.getClan(clanId).incGold(gold);
		} finally {
			TrackerManager.trackerClanGold(clanId, userId, gold);
		}
	}

	/**
	 * Уменьшает кол-во золота в клане по его ID и заносит действие в трекер
	 *
	 * @param clanId ID клана
	 * @param userId ID пользователя
	 * @param gold   кол-во золота, на которое надо увеличить
	 *
	 * @return доступное кол-во золота после увеличения.
	 * @see Clan#decGold(int)
	 */
	public int decGold(long clanId, long userId, int gold) {
		try {
			return ClanManager.getClan(clanId).decGold(gold);
		} finally {
			TrackerManager.trackerClanGold(clanId, userId, -gold);
		}
	}

	/**
	 * Устанавливает кол-во золота в клане на 0
	 *
	 * @param clanId ID клана
	 */
	public void cleanClanGold(long clanId) {
		ClanManager.getClan(clanId).setGold(0);
	}
}
