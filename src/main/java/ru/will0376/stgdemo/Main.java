package ru.will0376.stgdemo;

import lombok.extern.log4j.Log4j2;
import ru.will0376.stgdemo.clan.Clan;
import ru.will0376.stgdemo.clan.ClanController;
import ru.will0376.stgdemo.clan.ClanManager;
import ru.will0376.stgdemo.database.MysqlDataBase;
import ru.will0376.stgdemo.loader.JsonFileLoader;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * Главный класс.
 * <p><h>Используется для тестов работоспособности кода.</h></p>
 * <p>Здесь могут использоваться не самые лучшие практики написания кода</p>
 */
@Log4j2
public class Main {
	/**
	 * Главный метод, который позволяет управлять приложением в рантайме.
	 * <p>Возможные команды:</p>
	 * <p>createNewClans - Создаёт 10 новых кланов и сохраняет их</p>
	 * <p>addGoldToClan X Y Z(разделитель - пробел) - Добавляет в клан X(clanId) Y золота, повторяя Z раз (проверка трекинга)</p>
	 * <p>checkthreads(или ct) - Проверка работоспособности синхронизации потоков. В самом конце выводит полученное кол-во
	 * золота и ожидаемое, устанавливая в 0 золото после выполнения.</p>
	 * <p>printclans - Выводит в консоль информацию о кланах(id -> gold)</p>
	 * <p>save - Сохраняет лист на диск {@link JsonFileLoader#saveList()}</p>
	 * <p>checkconnection - Проверяет подключение к базе данных по данным из конфигурации {@link Configuration#databaseConfig}
	 * </p>
	 * <p>exit(или quit или stop) - Сохраняет лиск кланов на диск и завершает работу приложения</p>
	 *
	 * @param args Игнорируется
	 *
	 * @throws Exception при любых ошибках
	 */
	public static void main(String[] args) throws Exception {
		ClanManager.loadList();
		while (true) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			System.out.println("Command:");
			ClanController clanController = new ClanController();
			String s = reader.readLine();
			String[] arg = null;
			if (s.contains(" ")) {
				arg = s.split(" ");
				s = arg[0];
			}
			switch (s) {
				case "createNewClans":
					long maxId = ClanManager.getFirstAvailableID();
					for (int i = 1; i <= 10; i++) {
						Clan clan = ClanManager.createNewClan("test-" + (maxId + i));
						ClanManager.saveClan(clan);
					}
					break;
				case "addGoldToClan":
					if (arg != null) {
						int clanid = Integer.parseInt(arg[1]);
						int gold = Integer.parseInt(arg[2]);
						int range = Integer.parseInt(arg[3]);
						IntStream.range(0, range).forEach(i -> {
							clanController.incGold(clanid, -1, gold);
						});
					}
					break;

				case "checkthreads":
				case "ct":
					final Clan clan = ClanManager.getClanList().stream().findFirst().get();
					System.out.printf("gold-in: %s \n", clanController.getGold(clan.getId()));
					ExecutorService executor = Executors.newFixedThreadPool(100);
					AtomicInteger test = new AtomicInteger(0);

					IntStream.range(0, 50000).forEach(i -> executor.execute(() -> {
						test.addAndGet(i);
						clanController.incGold(clan.getId(), -1, i);
						int syncGold = clanController.getGold(clan.getId());

						System.out.println("iter: " + i + " " + Thread.currentThread().getName() + " gold: " + syncGold);
					}));
					stopExecutor(executor);
					System.out.printf("gold-out: %s \n", clanController.getGold(clan.getId()));
					System.out.printf("test: %s \n", test.get());
					clanController.cleanClanGold(clan.getId());
					break;
				case "printclans":
					ClanManager.getClanList()
							.forEach(e -> System.out.println(e.getId() + " -> " + clanController.getGold(e.getId())));
					break;
				case "save":
					ClanManager.saveList();
					break;
				case "checkconnection":
					MysqlDataBase.INSTANCE.checkTrackTable();
					break;
				case "exit":
				case "quit":
				case "stop":
					ClanManager.saveList();
					System.exit(0);
					break;
			}
		}

	}

	/**
	 * Останавливает потоки и ждёт их завершения 60 секунд, иначе убивает принудительно
	 *
	 * @param executor Сервис с потоками
	 */
	public static void stopExecutor(ExecutorService executor) {
		try {
			executor.shutdown();
			executor.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			System.err.println("termination interrupted");
		} finally {
			if (!executor.isTerminated()) {
				System.err.println("killing non-finished tasks");
			}
			executor.shutdownNow();
		}
	}
}
